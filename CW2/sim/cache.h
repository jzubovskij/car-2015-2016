#ifndef _CACHE_H_
#define _CACHE_H_

#include <iostream>
#include "instruction.h"
#include <bitset>
#include <math.h>
#include <map>
#include <deque>

//constants to denote easier access to states
const uint initial = 0;
const uint steady = 1;
const uint transient = 2;
const uint noPrediction = 3;

//constant denotign the stall cycles on a miss
const uint32_t CACHE_MISS_NUMBER_OF_STALLS = 4;
extern uint32_t prefetcher_policy;
extern uint32_t cache_size;
extern uint32_t prefetcher_type;
extern uint32_t cache_assoc;
extern uint32_t cacheline_size;
extern uint32_t prefetcher_lines;

//enum holding values for request types
enum CacheRequestType {
	CacheReadReq, CacheWriteReq, CacheWriteBackReq, CachePrefetchReq
};

//the struct to hold the RPT entry struct
struct RPTEntry {
	//the address of the access
	uint addr;
	//the stride of the access
	uint stride;
	//the state representation
	uint state;
	//the method that makes the correct transition for the RPT entry according to the STRIDE FSM
	void transition(bool hit, int stride);
public:
	bool isSteady() {
		return state == steady;
	}

	bool isTransient() {
		return state == transient;
	}
};

//the class for the prefetch queue entry
class CachePrefetch {
public:

	//address of the memory to be prefetched
	uint addr;
	//cycles number when the value would be actually available
	uint cycleReadyAt;

	//the constructor for the class
	CachePrefetch(uint addr, uint cyclereadyAt) {
		this->addr = addr;
		this->cycleReadyAt = cyclereadyAt;
	}
};

struct CachePacket {
	uint32_t pc;
	uint32_t addr;
	CacheRequestType reqType;		//0: read, 1: write, 2: prefetch
	uint32_t size;						//1: BYTE 4: DWORD
	uint32_t data;
	uint32_t readyCycleCount;

	CachePacket(uint32_t curCycle, uint32_t _pc, uint32_t _addr, CacheRequestType _reqType, uint32_t _size, uint32_t _data = 0) {
		pc = _pc;
		addr = _addr;
		reqType = _reqType;
		size = _size;
		data = _data;

		readyCycleCount = curCycle;
	}
};

//the class to hold the blocks in the cache
class CacheBlock {

	std::bitset<32 * 8> data; //as a char is 8 bits = byte

public:
	//the tag of the block
	uint32_t tag;
	//the cycle whe nit was last used (for LRU)
	uint32_t usedAtCycle;
	//boleans ot record if the block is valid, prefected / normal access, dirty
	bool valid;
	bool prefetched;
	bool dirty;

	//the constructor for the class
	CacheBlock() {
		//initializes a block to invalid, neved used etc.
		usedAtCycle = 0;
		tag = -1;
		valid = false;
		dirty = false;
		prefetched = false;
	}

	//method returning if block is valid
	bool isValid() {
		return valid;
	}

	//method returning if the block is dirty
	bool isDirty() {
		return dirty;
	}

	//method returning the dirty value
	void markDirty() {
		dirty = true;
	}

};

class SimpleCache {

public:
	//varaible holding the cache size
	uint32_t cacheSize;
	//variable holding the cache associativity
	uint32_t cacheAssoc;
	//varaible holding the cachelien size
	uint32_t cachelineSize;
	//variable holding the number of sets
	uint32_t setCount;
	//varaible holding the number of blocks
	uint32_t blockCount;
//varaible holding the size of the tag
	uint32_t tagSize;
	//varaible holding the size of the index
	uint32_t indexSize;
	//varaible holding the size of the offset
	uint32_t offsetSize;
	//varaible holding preftecher_type == 1 prefetcher policy
	uint32_t prefetcherPolicy;
	//the array of cache blocks
	CacheBlock* blocks;
	//constructor for the class
	SimpleCache(uint32_t cache_size, uint32_t cache_assoc, uint32_t cacheline_size) {
		//copy parameters into local variables
		cacheSize = cache_size;
		cacheAssoc = cache_assoc;
		cachelineSize = cacheline_size;
		prefetcherPolicy = prefetcher_type;

		//claculate block count
		blockCount = cacheSize / cachelineSize;
		//calculate set count
		setCount = blockCount / cacheAssoc;
		//initalize cache block array
		blocks = new CacheBlock[blockCount];

		//get address size
		indexSize = (uint32_t) log2(setCount);
		//get offset size
		offsetSize = (uint32_t) log2(cachelineSize);
		//get tag size
		tagSize = cachelineSize - indexSize - offsetSize;

	}
	//destructor for the class
	~SimpleCache() {
		delete[] blocks;
	}
	//function returning the index based on the current cache parameters and the address in parameters
	uint32_t getIndex(uint32_t addr);
	//function returning the index based on the current cache parameters and the address in parameters
	uint32_t getTag(uint32_t addr);
	//function returning a cache block reference for specified memory address (if such is in the cache)
	CacheBlock* access(uint addr, uint cycleOfAccess, bool isUsed);
	//function returning LRU block in the set (calculated from the address)
	CacheBlock* findVictim(uint32_t addr);
	//function that inserts the memory data into the block passed in as argument
	CacheBlock* insertBlock(uint addr, CacheBlock* victimBlk, uint currentCycle, bool prefetched);

};

//varaible holding the cache class instantiation
extern SimpleCache* cache;

//class designed to contorl the cache
class CacheCtrl {
public:
	//variable holding the cache
	SimpleCache* cache;
	//varaible to hold the reference to the cpu core
	cpu_core* core;
	CachePacket* pendingMissReq;
	std::deque<CachePacket*> requestQueue;

	//the queue holding the memory blocks being prefetched
	std::deque<CachePrefetch*> prefetchQueue;
	//the mao for the RPT
	std::map<uint, RPTEntry> RPT;

	uint32_t hits;
	uint32_t misses;

	void responseToCPU(CachePacket *pkt);
	void handleCacheFill(CachePacket *pkt);

	//method filling the prefetcher_policy == 1, cache as much as it can based on argument values
	void fillPrefetcher(uint addr, uint readyCycleCount);
	//method filling the prefetcher_policy == 2, cache with new prefetch request based on arguemnt values
	void fillPrefetcherStride(uint addr, uint readyCycleCount, uint stride);
	//the method handling the stride prefetching
	void stridePrefetch(CachePacket* pkt);
	void cycle();
	void access(CachePacket *pkt);
	void printStat();

	//class constructor
	CacheCtrl(cpu_core *c) {
		core = c;
		pendingMissReq = NULL;
		hits = 0;
		misses = 0;

		if (cache_size > 0) {
			printf("Cache Config: Size = %d b, Assoc = %d, Line Size = %d b, Prefetcher = %s\n", cache_size, cache_assoc, cacheline_size,
					prefetcher_type > 0 ? "enabled" : "disabled");
			cache = new SimpleCache(cache_size, cache_assoc, cacheline_size);
		} else {
			cache = NULL;
		}
	}
	//class destructor
	~CacheCtrl() {
		if (cache) {
			prefetchQueue.clear();
			delete cache;
		}
	}

};

#endif /* _CACHE_H_ */
