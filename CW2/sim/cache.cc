#include "cpu.h"
#include "stages.h"
#include <cstdio>
#include <cassert>
#include <iostream>
#include "cache.h"

void CacheCtrl::cycle() {
	if (!requestQueue.empty()) {
		assert(requestQueue.size() == 1);

		CachePacket* pkt = requestQueue.front();

		//check if there is a block with the indicated memory (address) in the cache
		CacheBlock *blk = cache->access(pkt->addr, pkt->readyCycleCount, true);

		stridePrefetch(pkt);

		//if such a block exiusts and is present in the cache
		if (blk && blk->isValid()) {
			//if it is a siple prefetcher
			if (prefetcher_type == 1) {
				//prefetch on a prefetched block hit if policy is 2 or if policy is 3
				if ((blk->prefetched && prefetcher_policy == 2) || prefetcher_policy == 3) {
					//we estimate the cycle a block is ready based on the current cpu cycles if the prefetcher queue is empty
					if (prefetchQueue.empty()) {
						fillPrefetcher(pkt->addr, core->cycles);
						//if we have prefetches stores in the queue, we estimate the cycle a block is ready based on the tail of the queue
					} else {
						fillPrefetcher(pkt->addr, prefetchQueue.back()->cycleReadyAt);
					}
				}
			}

			responseToCPU(pkt);
			requestQueue.pop_front();
			hits++;

		} else {
			//If there is a pendingMissReq, we should retry it in the next cycle.
			//This will not happen before you implement the prefetcher.
			if (pendingMissReq == NULL) {			//only support one outstanding miss request
				if (prefetchQueue.empty()) {
					//handle cache miss
					pendingMissReq = pkt;
					pendingMissReq->readyCycleCount += CACHE_MISS_NUMBER_OF_STALLS;

					//if for the simple prefetcher and for any policy, prefetch on a miss
					if (prefetcher_type == 1) {

						if (prefetcher_policy > 0) {
							fillPrefetcher(pendingMissReq->addr, pendingMissReq->readyCycleCount);
						}
					}

					requestQueue.pop_front();
					misses++;
				}
			}
		}

	}
	//serice miss request first
	if (pendingMissReq && (core->cycles >= pendingMissReq->readyCycleCount)) {

		handleCacheFill(pendingMissReq);
		responseToCPU(pendingMissReq);
		pendingMissReq = NULL;

	}			//otherwise if not servicing miss request, service a prefetch (if such exist)
	else if (!prefetchQueue.empty() && prefetchQueue.front()->cycleReadyAt <= core->cycles) {
		//get the address from the queue
		uint addr = prefetchQueue.front()->addr;
		//get the citim block
		CacheBlock *block = cache->findVictim(addr);
		//insert the prefetched block into the cache (prefetched = true)
		cache->insertBlock(addr, block, core->cycles, true);
		//remove block fro mthe queue
		prefetchQueue.pop_front();
	}
}

void CacheCtrl::access(CachePacket *pkt) {
	if (cache) {

		requestQueue.push_back(pkt);
	} else {
		pendingMissReq = pkt;
		pendingMissReq->readyCycleCount += CACHE_MISS_NUMBER_OF_STALLS;
		misses++;
	}
}

void CacheCtrl::handleCacheFill(CachePacket *pkt) {

	CacheBlock *victimBlk = cache->findVictim(pkt->addr);
	if (victimBlk->isValid() && victimBlk->isDirty())
		;

	//insert the block into the cache (prefetched = false) as normal access
	victimBlk = cache->insertBlock(pkt->addr, victimBlk, pkt->readyCycleCount, false);

	if (pkt->reqType == CacheWriteReq)
		victimBlk->markDirty();
}

void CacheCtrl::responseToCPU(CachePacket *pkt) {
	//NOTE: You do not need to simulate the data in the cache.
	//This is a hack for you to get the data from memory.
	if (pkt->reqType == CacheWriteReq) {
		if (pkt->size == 1) {
			core->mem->set<byte>(pkt->addr, pkt->data);
		} else if (pkt->size == 4) {
			core->mem->set<uint32_t>(pkt->addr, pkt->data);
		} else {
			assert(1);
		}
	} else if (pkt->reqType == CacheReadReq) {
		if (pkt->size == 1) {
			pkt->data = core->mem->get<byte>(pkt->addr);
		} else if (pkt->size == 4) {
			pkt->data = core->mem->get<uint32_t>(pkt->addr);
		} else {
			assert(1);
		}
	} else {
		assert(1);
	}

	core->mys.response(pkt);
}

void CacheCtrl::printStat() {
	//print the statistic here
	printf("stat.dcache_hits: %d\n", hits);
	printf("stat.dcache_misses: %d\n", misses);
	printf("stat.dcache_miss_rate: %.2f %%\n", ((double) misses / (double) (hits + misses)) * 100);
}

uint32_t SimpleCache::getIndex(uint32_t addr) {
	//get the according bits from the address which represent the index
	return (addr >> (offsetSize)) & (int) (pow(2, indexSize) - 1);
}
uint32_t SimpleCache::getTag(uint32_t addr) {
	//get the according bits from the address which represent the tag
	return (addr >> (indexSize + offsetSize)) & (int) (pow(2, tagSize) - 1);
}
CacheBlock* SimpleCache::access(uint addr, uint cycleOfAccess, bool isUsed) {
	//get index
	uint32_t index = getIndex(addr);
	//get tag
	uint32_t tag = getTag(addr);
	//search the set (index)
	for (uint32_t i = 0; i < cacheAssoc; i++) {
		//if we find the block with the same tag as our address
		if (blocks[index * cacheAssoc + i].tag == tag) {
			//update the LRU value for this block is it is beign used, nto just checked (prefetcher)
			if (isUsed) {
				blocks[index * cacheAssoc + i].usedAtCycle = cycleOfAccess;
			}
			//return the result if found
			return &blocks[index * cacheAssoc + i];
		}
	}
	//if no result found, return NULL
	return NULL;
}

CacheBlock* SimpleCache::findVictim(uint32_t addr) {
	//get index
	uint32_t index = getIndex(addr);
	//start search at the first index in the set
	uint32_t result = index * cacheAssoc;
	//search the set
	for (uint32_t i = 0; i < cacheAssoc; i++) {
		//if a block is not valid, return it immediately
		if (!blocks[cacheAssoc * index + i].valid) {
			return &blocks[cacheAssoc * index + i];
		}
		//otherwise, search for the LRU one
		if ((blocks[cacheAssoc * index + i].usedAtCycle < blocks[result].usedAtCycle)) {
			result = cacheAssoc * index + i;
		}
	}
	//block is now deprecated, until replaced with an insertion
	blocks[result].valid = false;
	//return the result
	return &blocks[result];
}

CacheBlock* SimpleCache::insertBlock(uint addr, CacheBlock* victimBlk, uint currentCycle, bool prefetched) {
	//get the new tag to be held in the block
	uint32_t tag = getTag(addr);
	//renew tag
	victimBlk->tag = tag;
	//block is now in the cache
	victimBlk->valid = true;
	//update LRU
	victimBlk->usedAtCycle = currentCycle;
	//update prefetched boolean
	victimBlk->prefetched = prefetched;
	return victimBlk;
}

void CacheCtrl::fillPrefetcher(uint addr, uint readyCycleCount) {
	//get initial value for stall value
	int readyAt = readyCycleCount + CACHE_MISS_NUMBER_OF_STALLS;
	//get initial value for memory address for prefetchign at by a block size
	int address = addr + cacheline_size;
	//variable to hold maximum number of elements we can insert without the queue exceeding the N-line size
	uint sizeToFill = prefetcher_lines - prefetchQueue.size();
	//attempt to insert said amount of (next) blocks into the prefetch queue
	for (uint i = 0; i < sizeToFill; i++) {

		//check if block already in the cahce, do not update LRU value as not using the actual "data"
		CacheBlock *temp = cache->access(address, readyAt, false);
		//if block is not in the cache
		if (!(temp && temp->isValid())) {
			//create new prefetch request
			CachePrefetch *block = new CachePrefetch(address, readyAt);
			//add said request to the prefetch queue
			prefetchQueue.push_back(block);
			//increment the stall value only if an element added to the queue
			readyAt += CACHE_MISS_NUMBER_OF_STALLS;
		}
		//increment potential prefetch address by a block size
		address += cacheline_size;
	}
}

void CacheCtrl::fillPrefetcherStride(uint addr, uint readyCycleCount, uint stride) {

	//get initial value for stall value
	int readyAt = readyCycleCount + CACHE_MISS_NUMBER_OF_STALLS;
	//get initial value for memory address for prefetchign at by a block size
	int address = addr + stride;
	//check if block already in the cahce, do not update LRU value as not using the actual "data"
	CacheBlock *temp = cache->access(address, readyAt, false);

	if (!(temp && temp->isValid())) {
		//create new prefetch request
		CachePrefetch *block = new CachePrefetch(address, readyAt);
		//add said request to the prefetch queue
		prefetchQueue.push_back(block);
	}

}

void CacheCtrl::stridePrefetch(CachePacket* pkt) {
	//if ti is a stride prefetcher
	if (prefetcher_type == 2) {
		//check if entry exists in the RPT table
		if (RPT.find(pkt->pc) != RPT.end()) {
			//if it exists there, then recalculate stride
			uint stride = pkt->addr - RPT[pkt->pc].addr;
			//if deoending on th strides mathcing, make a state tansition
			if (RPT[pkt->pc].stride != stride) {
				RPT[pkt->pc].transition(false, stride);
			} else {
				RPT[pkt->pc].transition(true, stride);
			}

			//renew address
			RPT[pkt->pc].addr = pkt->addr;
			//if the stride is non-zero prefetch the block of addr+stride
			if ((RPT[pkt->pc].stride != 0 && RPT[pkt->pc].isSteady()) || RPT[pkt->pc].isTransient()) {
				int readyCycle = prefetchQueue.empty() ? core->cycles : prefetchQueue.back()->cycleReadyAt;
				fillPrefetcherStride(RPT[pkt->pc].addr, readyCycle, RPT[pkt->pc].stride);
			}
			//otherwise add the entry to the table
		} else {
			RPT[pkt->pc].addr = pkt->addr;
			RPT[pkt->pc].stride = 0;
			RPT[pkt->pc].state = 0;
		}
	}
}
void RPTEntry::transition(bool hit, int stride) {
	//do not update stride only if we are in steady state befor the unequal stride transition
	if (!hit && state != steady) {
		this->stride = stride;
	}
	//transition to either steady or rtansient state
	if (state == initial) {
		state = hit ? steady : transient;
		//stansition to either steady or initla state
	} else if (state == steady) {
		state = hit ? steady : initial;
		//transition either setady or noPrediction state
	} else if (state == transient) {
		state = hit ? steady : noPrediction;
		//transition to either transient or noPrediction state
	} else if (state == noPrediction) {
		state = hit ? transient : noPrediction;

	}
}

